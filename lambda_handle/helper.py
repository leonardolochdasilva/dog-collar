"""JSON helper module"""
import datetime
import decimal
import json


class JsonEncoderExtension(json.JSONEncoder):
    """Extends the original JSON encoder to handle date and datetime objects"""
    def default(self, o):  # pylint: disable=method-hidden
        """Encodes object as JSON"""
        if isinstance(o, datetime.datetime):
            return str(o)
        if isinstance(o, datetime.date):
            return str(o)
        if isinstance(o, decimal.Decimal):
            return float(o)
        return super().default(o)


class JsonHelper:
    """Implements commonly used JSON operations"""
    @staticmethod
    def dumps(input_object, **kwargs):
        """Dumps object to JSON using extended encoder"""
        return json.dumps(input_object, cls=JsonEncoderExtension, **kwargs)
