"""HTTP response builders"""
from .helper import JsonHelper


class ResponseBuilder:
    """Builds a response to a HTTP request"""
    def __init__(self):
        self._is_base64_encoded = False
        self._status_code = None
        self._headers = {}
        self._multi_value_headers = {}
        self._body = None

    def body(self, value, json_encode=True):
        """Sets the response body. Also configures the content type header if the body is a json."""
        self._body = JsonHelper.dumps(value) if value else None
        self.header("Content-Type", "application/json")
        return self

    def header(self, name, value):
        """Adds a header to the response."""
        self._headers[name] = value
        return self

    def status_code(self, value):
        """Sets the response HTTP status code"""
        self._status_code = int(value)

    def build(self):
        """Serializes the response to the expected format."""

        response = {
            "isBase64Encoded": self._is_base64_encoded,
            "statusCode": self._status_code,
            "headers": self._headers,
            "multiValueHeaders": self._multi_value_headers,
            "body": self._body
        }
        return response


class CreatedBuilder(ResponseBuilder):
    """Builds a created HTTP response"""
    def __init__(self, body=None, json_encode=True):
        super().__init__()
        self.status_code(201)
        self.body(body, json_encode)


class BadRequestBuilder(ResponseBuilder):
    """Builds a bad request HTTP response"""
    def __init__(self, error_message):
        super().__init__()
        self.status_code(400)
        self.body({"error": error_message})


class InternalServerErrorBuilder(ResponseBuilder):
    """Builds an internal server error HTTP response"""
    def __init__(self):
        super().__init__()
        self.status_code(500)
        self.body({"error": "Internal server error"})


class OkBuilder(ResponseBuilder):
    """Builds an Ok HTTP response"""
    def __init__(self, body, json_encode=True):
        super().__init__()
        self.status_code(200)
        self.body(body, json_encode)
