import json
from enum import Enum


class HttpMethods(Enum):
    """The available HTTP methods"""
    GET = 'GET'
    DELETE = 'DELETE'
    PATCH = 'PATCH'
    POST = 'POST'
    PUT = 'PUT'


class Request:
    """Class that represents a HTTP request."""
    def __init__(self, event):
        self._query_parameters = self._get_value_if_not_null(event, "queryStringParameters", {})
        self._path_parameters = self._get_value_if_not_null(event, "pathParameters", {})
        self._body = json.loads(event["body"]) if event["body"] else None

    def get_query_parameter(self, parameter_name, default_value=None):
        """
        Gets the value of a query parameter.
        Returns the default value if the parameter doesn't exist.
        """
        return self._query_parameters.get(parameter_name, default_value)

    @property
    def body(self):
        """The request body"""
        return self._body

    def get_path_parameter(self, parameter_name, default_value=None):
        """
        Gets the value of a path parameter.
        Returns the default value if the parameter doesn't exist.
        """
        return self._path_parameters.get(parameter_name, default_value)

    @classmethod
    def _get_value_if_not_null(cls, event, parameter, default_value):
        return event[parameter] if event[parameter] else default_value
