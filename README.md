# Dog Collar

## About
Dog Collar is a project that consists of a cloud platform for storing data generated through dog monitoring. The system has three endpoints, one to insert the information, one to list all saved reports and finally one to list by the dog's id.

## Infrastruct
For the development of the project, Amazon Web Service (AWS) was used as a cloud service. Among the services, Api GateWay, Lambda and DynamoDb were used. The entire infrastructure built was developed through Terraform, Iaas (Infrastructure as a Service).

## Database
This are the fields that will save in database:

dog_id | create  | physical_activity  | location| barking


The dog_id field is informed in path_string, created automatically when the record is saved and the others are informed in the request body

## Programming Language
In the construction of the endpoints, the chosen language was python version 3.7.3

## QA (Quality)
To guarantee the quality of the code, several test scenarios were developed that could cover 100% of the code and the code style was guaranteed. For this, pytest was used, for tests, coverage and flake8. To verify this just
the run the following script

```sh
$ make
```

## Security
To keep this data safe and away from possible malicious access, to interact with the system you need to authenticate using AWS Signature, having the AWS credentials.

## Create the Environment
Right after the project clone, inside the root folder the following commands must be made:

```sh
$ make create-venv
```
With this command all dependencies must be installed in the virtual environment created, but there is still a dependency to be installed, Terraform, to install follow the steps.

```sh
$ wget https://releases.hashicorp.com/terraform/{version}/terraform_{version}_linux_amd64.zip

$ unzip terraform_{version}_linux_amd64.zip

$ chmod +x terraform

$ sudo mv terraform /usr/local/bin/

```

The tag '{version}' represents the current version of Terraform and can be found at https://www.terraform.io/downloads.html, and to confirm:

```sh
$ terraform --version
```

## Deploy
To deploy the infrastructure and code, you need to do the following steps:
```sh
$ cd infrastructure/

$ terrafom init

$ terraform apply --auto-approve
```
And ready, to access interact just fill the following url with the data referring to https://{API-ID}.execute-api.us-east-1.amazonaws.com/v1/report-colla. Remembering that AWS Cli must be configured to have permission to deploy.

## Documentation
The project documentation with the endpoints is:

https://gitlab.com/leonardolochdasilva/dog-collar/-/blob/master/infrastructure/swagger.yaml