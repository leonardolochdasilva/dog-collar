from enum import Enum

import boto3


from botocore.exceptions import ClientError


class AttributeFilter(Enum):
    KeyConditionExpression = 'KeyConditionExpression'
    FilterExpression = 'FilterExpression'


class DynamoDbModel:
    def __init__(self, table_name):
        dynamodb = boto3.resource('dynamodb')
        self.table = dynamodb.Table(table_name)
        self.table_name = table_name

    def _create(self, kwargs):
        try:
            response = self.table.put_item(Item=kwargs)
        except ClientError as e:
            raise self._raise(self._get_message_error(self._create.__name__, str(e)))
        return response

    def _list(self, filters=None):
        try:
            response = self.table.scan(**(filters or {}))
            data = response['Items']
            while 'LastEvaluatedKey' in response:
                filters.update({'ExclusiveStartKey': response['LastEvaluatedKey']})
                response = self.table.scan(**(filters or {}))
                data.extend(response['Items'])

        except ClientError as e:
            raise self._raise(self._get_message_error(self._list.__name__, str(e)))

        return data

    def _list_by_primary_key(self, filters):
        try:
            response = self.table.query(
                **filters
            )
            data = response['Items']
            while 'LastEvaluatedKey' in response:
                filters.update({'ExclusiveStartKey': response['LastEvaluatedKey']})
                response = self.table.query(**filters)
                data.extend(response['Items'])

        except ClientError as e:
            raise self._raise(self._get_message_error(self._list_by_primary_key.__name__, str(e)))

        return response['Items']

    def _get_message_error(self, action, additional_message=''):
        return f'Error during {action} item in {self.table_name}: {additional_message}'
