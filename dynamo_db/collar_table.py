import os
from datetime import datetime
from boto3.dynamodb.conditions import Key  # noqa


from .mixins import DynamoDbModel, AttributeFilter


class CollarTableModelError(Exception):
    pass


class CollarTableModel(DynamoDbModel):
    def __init__(self):
        permission_table_name = os.environ.get('dog_collar_table_name')
        super().__init__(permission_table_name)

    def create(self, dog_id, physical_activity, location, barking):
        collar_data = {
            "dog_id": dog_id,
            "created": str(datetime.now()),
            "physical_activity": physical_activity,
            "location": location,
            "barking": barking
        }
        self._create(collar_data)
        return True

    def list_by_dog_id(self, dog_id, start_period, end_period):
        field_filter = [f"Key('dog_id').eq('{dog_id}')"]
        attr_call = AttributeFilter.KeyConditionExpression.value
        if start_period:
            field_filter.append(f"Key('created').gte('{start_period}')")
            attr_call = AttributeFilter.FilterExpression.value

        if end_period:
            field_filter.append(f"Key('created').lte('{end_period}')")
            attr_call = AttributeFilter.FilterExpression.value

        filters = {attr_call: eval(' & '.join(field_filter))}

        if attr_call == AttributeFilter.FilterExpression.value:
            results = self._list(filters)
        else:
            results = self._list_by_primary_key(filters)
        return results

    def list(self, start_period, end_period):
        attr_call = AttributeFilter.FilterExpression.value
        field_filter = []
        if start_period:
            field_filter.append(f"Key('created').gte('{start_period}')")

        if end_period:
            field_filter.append(f"Key('created').lte('{end_period}')")

        filters = {}
        args_query = ' & '.join(field_filter)
        if args_query:
            filters = {attr_call: eval(args_query)}
        return self._list(filters)

    def _raise(self, message):
        raise CollarTableModelError(message)
