variable "service" {
  description = "The name of the service"
  type        = string
}

variable "api_version" {
  description = "The API version (only alphanumeric characters are allowed)"
  type        = string
}

variable "lambda_arns" {
  description = "The lambda function ARNs to be called by the API Gateway"
  type        = map
}

variable "aws_region" {
  description = "The current AWS region"
  type        = string
}

variable "account_id" {
  description = "The current AWS account"
  type        = string
}

variable "api_name" {
  description = "The api name to be added to the base URL"
  type        = string
}