data "template_file" api_swagger {
  template = file("${path.root}/swagger.yaml")

  vars = merge(var.lambda_arns,
    {
      aws_region = var.aws_region
      api_name   = "${var.api_name}"
    }
  )
}


resource "aws_api_gateway_rest_api" "this" {
  name = var.service
  body = data.template_file.api_swagger.rendered
  endpoint_configuration {
    types = ["EDGE"]
  }
}

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  stage_name  = var.api_version
  variables = {
    hash = md5(data.template_file.api_swagger.rendered)
  }
}

resource "aws_lambda_permission" "api_gw_invocation" {
  count         = length(var.lambda_arns)
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = values(var.lambda_arns)[count.index]
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.aws_region}:${var.account_id}:${aws_api_gateway_rest_api.this.id}/*/*"
}