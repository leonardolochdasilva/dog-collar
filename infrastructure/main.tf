provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}


### LAMBDA CONFIG ###
resource "aws_s3_bucket" "bucket-dog-collar" {
  bucket = "dog-collar"
  acl    = "private"
}

module "create_collar_report" {
  source         = "./lambda"
  service        = "dog_collar"
  function_name  = "create_collar_report"
  lambda_handler = "dog_collar.endpoints.create_collar_report"
  source_files   = ["dog_collar", "lambda_handle", "dynamo_db"]
  requirements   = "requirements/base.txt"
  bucket         = aws_s3_bucket.bucket-dog-collar.bucket
  env_vars = {
    dog_collar_table_name = module.dynamodb.dog_collar_table_name
  }
  additional_policies = [
    aws_iam_policy.policy_access_dynamodb.arn
  ]
  timeout            = 60
}

module "list_collar_report_by_dog_id" {
  source         = "./lambda"
  service        = "dog_collar"
  function_name  = "list_collar_report_by_dog_id"
  lambda_handler = "dog_collar.endpoints.list_collar_report_by_dog_id"
  source_files   = ["dog_collar", "lambda_handle", "dynamo_db"]
  requirements   = "requirements/base.txt"
  bucket         = aws_s3_bucket.bucket-dog-collar.bucket
  env_vars = {
    dog_collar_table_name = module.dynamodb.dog_collar_table_name
  }
  additional_policies = [
    aws_iam_policy.policy_access_dynamodb.arn
  ]
  timeout            = 60
}

module "list_collar_report" {
  source         = "./lambda"
  service        = "dog_collar"
  function_name  = "list_collar_report"
  lambda_handler = "dog_collar.endpoints.list_collar_report"
  source_files   = ["dog_collar", "lambda_handle", "dynamo_db"]
  requirements   = "requirements/base.txt"
  bucket         = aws_s3_bucket.bucket-dog-collar.bucket
  env_vars = {
    dog_collar_table_name = module.dynamodb.dog_collar_table_name
  }
  additional_policies = [
    aws_iam_policy.policy_access_dynamodb.arn
  ]
  timeout            = 60
}

### API GATEWAY CONFIG ###
locals {
  aws_region    = data.aws_region.current.name
  account_id    = data.aws_caller_identity.current.account_id
  service       = "dog-collar"
  prefix        = local.service
}

module "api_gateway" {
  source      = "./api_gateway"
  service     = local.service
  api_version = "v1"
  aws_region  = local.aws_region
  account_id  = local.account_id
  api_name    = "dog-collar"
  lambda_arns  = {
    create_collar_report = module.create_collar_report.this_lambda_function_arn
    list_collar_report_by_dog_id = module.list_collar_report_by_dog_id.this_lambda_function_arn
    list_collar_report = module.list_collar_report.this_lambda_function_arn
  }
}

### DYNAMODB CONFIG ###
module "dynamodb" {
  source                = "./dynamodb"
}

data "aws_iam_policy_document" "data_policy_access_dynamodb" {
  statement {
    effect = "Allow"
    actions = [
      "dynamodb:PutItem",
      "dynamodb:DeleteItem",
      "dynamodb:GetItem",
      "dynamodb:Scan",
      "dynamodb:Query",
      "dynamodb:UpdateItem",
    ]
    resources = [
      module.dynamodb.dog_collar_table_arn,
    ]
  }
}

resource "aws_iam_policy" "policy_access_dynamodb" {
  name   = "${local.service}-dynamodb"
  path   = "/"
  policy = data.aws_iam_policy_document.data_policy_access_dynamodb.json
}
