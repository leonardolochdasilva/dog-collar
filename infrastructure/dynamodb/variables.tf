variable "amount_capacity_read" {
  description = "Amount capacity read"
  type        = number
  default     = 10
}

variable "amount_capacity_write" {
  description = "Amount capacity write"
  type        = number
  default     = 10
}

variable "billing_mode" {
  description = "Billing mode"
  type        = string
  default     = "PAY_PER_REQUEST"
}

