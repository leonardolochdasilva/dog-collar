resource "aws_dynamodb_table" "dog_collar_table" {
  name           = "DOG-COLLAR"
  billing_mode   = var.billing_mode
  read_capacity  = var.amount_capacity_read
  write_capacity = var.amount_capacity_write
  hash_key       = "dog_id"
  range_key      = "created"

  attribute {
    name = "dog_id"
    type = "S"
  }

  attribute {
    name = "created"
    type = "S"
  }

  ttl {
    attribute_name = ""
    enabled        = false
  }
}
