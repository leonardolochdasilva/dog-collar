output "dog_collar_table_name" {
  description = "The dog collar table name"
  value       = aws_dynamodb_table.dog_collar_table.name
}

output "dog_collar_table_arn" {
  description = "The dog collar table arn"
  value       = aws_dynamodb_table.dog_collar_table.arn
}
