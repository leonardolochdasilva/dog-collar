import os
import logging
import jsonschema

from dynamo_db.collar_table import CollarTableModel

logger = logging.getLogger(__name__)
logger.setLevel(getattr(logging, os.environ.get('log_level', 'INFO')))
logger.info("Dog collar services logger")


class DogCollarService:
    @staticmethod
    def validate_body(data: dict, schema: dict):
        if schema is None:
            return False
        jsonschema.validate(data, schema, format_checker=jsonschema.FormatChecker())
        return True

    @staticmethod
    def create_collar_report(dog_id: str, collar_report_data: dict):
        CollarTableModel().create(dog_id, **collar_report_data)

    @staticmethod
    def list_collar_report_by_dog_id(dog_id: str, start_period=None, end_period=None):
        return CollarTableModel().list_by_dog_id(dog_id, start_period, end_period)

    @staticmethod
    def list_collar_report(start_period=None, end_period=None):
        return CollarTableModel().list(start_period, end_period)
