import json
from copy import deepcopy
from datetime import datetime

from botocore.exceptions import ClientError

from dog_collar.endpoints import create_collar_report, list_collar_report_by_dog_id, list_collar_report

RETURN_LIST_DOG_COLLAR = [
    {
        "dog_id": "fake_id",
        "created": "2020-03-12 08:45:08.838889",
        "location": "location",
        "barking": "barking",
        "physical_activity": "physical_activity"
    },
    {
        "dog_id": "fake_id",
        "created": "2020-03-12 08:46:10.250385",
        "location": "location",
        "barking": "barking",
        "physical_activity": "physical_activity"
    }
]

RESPONSE_LIST_ITEM = {
    'Items': [RETURN_LIST_DOG_COLLAR, RETURN_LIST_DOG_COLLAR],
    'Count': 2,
}


def test_should_create_report_collar(environment, request_builder, mock_boto):
    #  GIVEN
    data = json.dumps({'physical_activity': 'physical_activity', 'barking': 'barking', 'location': 'location'})
    request_builder.pathParameters = {'dog_id': 'fake_id'}
    request_builder.body = data
    request_builder.httpMethod = 'POST'

    #  WHEN
    response = create_collar_report(request_builder, None)
    #  THEN
    assert response['statusCode'] == 201


def test_should_not_create_report_collar_miss_data(environment, request_builder, mock_boto):
    #  GIVEN
    data = json.dumps({})
    request_builder.pathParameters = {'dog_id': 'fake_id'}
    request_builder.body = data
    request_builder.httpMethod = 'POST'

    #  WHEN
    response = create_collar_report(request_builder, None)
    #  THEN
    assert response['statusCode'] == 400


def test_should_not_create_report_due_error_save_data(environment, request_builder, mock_boto):
    #  GIVEN
    data = json.dumps({'physical_activity': 'physical_activity', 'barking': 'barking', 'location': 'location'})
    request_builder.pathParameters = {'dog_id': 'fake_id'}
    request_builder.body = data
    request_builder.httpMethod = 'POST'

    error_response = {'Error': {'Code': 'Something else'}}
    mock_boto.resource.return_value.Table.return_value.put_item.side_effect = ClientError(error_response, '')

    #  WHEN
    response = create_collar_report(request_builder, None)

    #  THEN
    assert response['statusCode'] == 500


def test_should_list_report_collar_by_dog_id_with_period(environment, request_builder, mock_boto):
    #  GIVEN
    request_builder.pathParameters = {'dog_id': 'fake_id'}
    request_builder.httpMethod = 'GET'
    request_builder.queryStringParameters = {'end_period': str(datetime.now()), 'start_period': '2020-03-12'}
    mock_boto.resource.return_value.Table.return_value.scan.return_value = RESPONSE_LIST_ITEM

    #  WHEN
    response = list_collar_report_by_dog_id(request_builder, None)

    #  THEN
    assert response['statusCode'] == 200
    assert RETURN_LIST_DOG_COLLAR in json.loads(response['body'])


def test_should_list_report_collar_by_dog_id(environment, request_builder, mock_boto):
    #  GIVEN
    request_builder.pathParameters = {'dog_id': 'fake_id'}
    request_builder.httpMethod = 'GET'
    mock_boto.resource.return_value.Table.return_value.query.return_value = RESPONSE_LIST_ITEM

    #  WHEN
    response = list_collar_report_by_dog_id(request_builder, None)

    #  THEN
    assert response['statusCode'] == 200
    assert RETURN_LIST_DOG_COLLAR in json.loads(response['body'])


def test_should_list_report_collar_by_dog_id_paginated(environment, request_builder, mock_boto):
    #  GIVEN
    request_builder.pathParameters = {'dog_id': 'fake_id'}
    request_builder.httpMethod = 'GET'

    return_value = deepcopy(RESPONSE_LIST_ITEM)
    return_value_paginated = deepcopy(RESPONSE_LIST_ITEM)
    return_value_paginated.update({'LastEvaluatedKey': RETURN_LIST_DOG_COLLAR[0]['dog_id']})
    mock_boto.resource.return_value.Table.return_value.query.side_effect = [return_value_paginated, return_value]

    #  WHEN
    response = list_collar_report_by_dog_id(request_builder, None)

    #  THEN
    assert response['statusCode'] == 200
    assert RETURN_LIST_DOG_COLLAR in json.loads(response['body'])


def test_should_not_list_report_collar_by_dog_id_due_error_list_data(environment, request_builder, mock_boto):
    #  GIVEN
    request_builder.pathParameters = {'dog_id': 'fake_id'}
    request_builder.httpMethod = 'GET'

    error_response = {'Error': {'Code': 'Something else'}}
    mock_boto.resource.return_value.Table.return_value.query.side_effect = ClientError(error_response, '')

    #  WHEN
    response = list_collar_report_by_dog_id(request_builder, None)

    #  THEN
    assert response['statusCode'] == 500


def test_should_list_report_collar_with_period(environment, request_builder, mock_boto):
    #  GIVEN
    request_builder.httpMethod = 'GET'
    request_builder.queryStringParameters = {'end_period': str(datetime.now()), 'start_period': '2020-03-12'}

    mock_boto.resource.return_value.Table.return_value.scan.return_value = RESPONSE_LIST_ITEM

    #  WHEN
    response = list_collar_report(request_builder, None)

    #  THEN
    assert response['statusCode'] == 200
    assert RETURN_LIST_DOG_COLLAR in json.loads(response['body'])


def test_should_list_report_collar(environment, request_builder, mock_boto):
    #  GIVEN
    request_builder.httpMethod = 'GET'
    mock_boto.resource.return_value.Table.return_value.scan.return_value = RESPONSE_LIST_ITEM

    #  WHEN
    response = list_collar_report(request_builder, None)

    #  THEN
    assert response['statusCode'] == 200
    assert RETURN_LIST_DOG_COLLAR in json.loads(response['body'])


def test_should_list_report_collar_paginated(environment, request_builder, mock_boto):
    #  GIVEN
    request_builder.httpMethod = 'GET'

    return_value = deepcopy(RESPONSE_LIST_ITEM)
    return_value_paginated = deepcopy(RESPONSE_LIST_ITEM)
    return_value_paginated.update({'LastEvaluatedKey': RETURN_LIST_DOG_COLLAR[0]['dog_id']})
    mock_boto.resource.return_value.Table.return_value.scan.side_effect = [return_value_paginated, return_value]

    #  WHEN
    response = list_collar_report(request_builder, None)

    #  THEN
    assert response['statusCode'] == 200
    assert RETURN_LIST_DOG_COLLAR in json.loads(response['body'])


def test_should_not_list_report_collar_due_error_get_data(environment, request_builder, mock_boto):
    #  GIVEN
    request_builder.httpMethod = 'GET'

    error_response = {'Error': {'Code': 'Something else'}}
    mock_boto.resource.return_value.Table.return_value.scan.side_effect = ClientError(error_response, '')

    #  WHEN
    response = list_collar_report(request_builder, None)

    #  THEN
    assert response['statusCode'] == 500
