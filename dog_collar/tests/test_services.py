import jsonschema
import pytest

from dog_collar.schemas import CREATE_REGISTER_SCHEMA
from dog_collar.services import DogCollarService


def test_should_validate_body():
    #  GIVEN
    data = {'physical_activity': 'physical_activity', 'barking': 'barking', 'location': 'location'}

    #  WHEN
    is_validate = DogCollarService.validate_body(data, CREATE_REGISTER_SCHEMA)

    #  THEN
    assert is_validate


def test_should_not_validate_body_miss_params():
    #  GIVEN
    #  WHEN
    with pytest.raises(jsonschema.ValidationError) as exc_info:
        DogCollarService.validate_body({}, CREATE_REGISTER_SCHEMA)

    # THEN
    assert exc_info.value.message == '\'physical_activity\' is a required property'


def test_should_not_validate_body_miss_schema():
    #  GIVEN
    data = {'physical_activity': 'physical_activity', 'barking': 'barking', 'location': 'location'}

    #  WHEN
    is_validate = DogCollarService.validate_body(data, None)

    # THEN
    assert not is_validate
