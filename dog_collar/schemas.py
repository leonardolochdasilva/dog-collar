CREATE_REGISTER_SCHEMA = {
    '$schema': 'http://json-schema.org/draft-06/schema#',
    'type': 'object',
    'required': ['physical_activity', 'barking', 'location'],
    'properties': {
        'physical_activity': {'type': 'string'},
        'barking': {'type': 'string'},
        'location': {'type': 'string'}
    }
}
