import logging
import os

import jsonschema

from dynamo_db.collar_table import CollarTableModelError
from lambda_handle.reponse_builder import BadRequestBuilder, InternalServerErrorBuilder, \
    CreatedBuilder, OkBuilder
from lambda_handle.request_builder import Request
from .services import DogCollarService
from .schemas import CREATE_REGISTER_SCHEMA

logger = logging.getLogger(__name__)
logger.setLevel(getattr(logging, os.environ.get('log_level', 'INFO')))
logger.info("Dog collar endpoint logger")


def create_collar_report(event, context):
    try:
        request = Request(event)
        dog_id = request.get_path_parameter(f'dog_id')
        DogCollarService.validate_body(request.body, CREATE_REGISTER_SCHEMA)
        DogCollarService.create_collar_report(dog_id=dog_id, collar_report_data=request.body)
        return CreatedBuilder().build()
    except jsonschema.ValidationError as error:
        return BadRequestBuilder(str(error)).build()
    except CollarTableModelError as error:
        logger.info(f'Error during create report collar: {error}')
        return InternalServerErrorBuilder().build()


def list_collar_report_by_dog_id(event, context):
    try:
        request = Request(event)

        dog_id = request.get_path_parameter(f'dog_id')
        start_period = request.get_query_parameter('start_period', None)
        end_period = request.get_query_parameter('end_period', None)

        response = DogCollarService.list_collar_report_by_dog_id(dog_id, start_period, end_period)
        return OkBuilder(response).build()
    except CollarTableModelError as error:
        logger.info(f'Error during create report collar: {error}')
        return InternalServerErrorBuilder().build()


def list_collar_report(event, context):
    try:
        request = Request(event)

        start_period = request.get_query_parameter('start_period', None)
        end_period = request.get_query_parameter('end_period', None)

        response = DogCollarService.list_collar_report(start_period, end_period)
        return OkBuilder(response).build()
    except CollarTableModelError as error:
        logger.info(f'Error during create report collar: {error}')
        return InternalServerErrorBuilder().build()
