import os
import pytest

from munch import munchify


@pytest.fixture
def environment():
    os.environ['dog_collar_table_name'] = "DOG-COLLAR"


@pytest.fixture
def request_builder():
    event = {
        'body': '',
        'pathParameters': '',
        'resource': '',
        'path': '',
        'httpMethod': '',
        'headers': '',
        'queryStringParameters': '',
    }
    return munchify(event)


@pytest.fixture
def mock_boto(mocker):
    mock = mocker.patch('dynamo_db.mixins.boto3')
    return mock
